/*
 * battery.h
 *
 *  Created on: Oct 10, 2020
 *      Author: kaczp
 */

#ifndef INC_BATTERY_H_
#define INC_BATTERY_H_

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#define VOLTAGE_DIVISION 0.7857f
#define BATTERY_REF_MAX VOLTAGE_DIVISION*4.2f/3.3f*4096
#define BATTERY_REF_MIN VOLTAGE_DIVISION*3.5f/3.3f*4096
#define BATTERY_RANGE BATTERY_REF_MAX-BATTERY_REF_MIN
#define BATTERY_TENTH_UNIT (BATTERY_REF_MAX-BATTERY_REF_MIN)/10.f;
#define BATTERY_BUFFER_SIZE 60

typedef struct
{
	osThreadId_t batteryTaskHandle;
	osSemaphoreId_t batteryBinarySemaphoreHandle;
	ADC_HandleTypeDef *hadc;
	TIM_HandleTypeDef *htim;
	uint8_t is_charged;
	uint8_t possible_charge_state_change_f;
	uint8_t samples_index;
	uint8_t samples_cnt;
	uint16_t samples_buf[BATTERY_BUFFER_SIZE];
	uint8_t adc_sampling;
	uint8_t percent;
} BATTERY_Controller;

BATTERY_Controller *BATTERY_GetBatteryController();
TIM_HandleTypeDef *BATTERY_GetBatteryTimerHandle(BATTERY_Controller *batc);
void BATTERY_CheckIfPluggedIn(BATTERY_Controller *batc);
void BATTERY_ControllerInit(BATTERY_Controller *batc, ADC_HandleTypeDef *hadc, TIM_HandleTypeDef *htim, osThreadId_t batteryTaskHandle, osSemaphoreId_t batteryBinarySemaphoreHandle);
void BATTERY_StartConversion(BATTERY_Controller *batc);
void BATTERY_StopConversion(BATTERY_Controller *batc);
void BATTERY_GetPercent(BATTERY_Controller *batc);
float BATTERY_GetReadingsAverage(BATTERY_Controller *batc);
uint8_t BATTERY_GetPowerLevel(BATTERY_Controller *batc);
uint8_t BATTERY_GetChargeState(BATTERY_Controller *batc);
float BATTERY_ConvertToVoltage(float sample);
void BATTERY_UpdatePowerLevelCharacteristic(BATTERY_Controller *batc);
void BATTERY_UpdatePowerStateCharacteristic(BATTERY_Controller *batc);
void BATTERY_ConversionCompleteCallback();
void BATTERY_ChargeStateChangeCallback();
#endif /* INC_BATTERY_H_ */
