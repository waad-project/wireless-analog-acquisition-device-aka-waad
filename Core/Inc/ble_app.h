///*
// * ble_app.h
// *
// *  Created on: Sep 26, 2020
// *      Author: Przemek
// */
//
//#ifndef INC_BLE_APP_H_
//#define INC_BLE_APP_H_
//
///* Includes */
//
///* Defines */
//#define WAAD_SHORT_NAME   	'W','A','A','D'
//#define WAAD_COMPLETE_NAME	'W','i','r','e','l','e','s','s',' ','A','n','a','l','o','g',' ','A','c','q','u','i','s','i','t','i','o','n',' ','D','e','v','i','c','e'
//
//#define SENSOR_DEMO_NAME_SIZE   4
//#define BDADDR_SIZE        6
//
///* Type declarations */
//
///* Function prototypes */
//void BLE_AppProcess(void);
//void BLE_MainTask(void);
//tBleStatus BLE_AppInit(void);
//tBleStatus BLE_AddServices(void);
//tBleStatus BLE_UpdateCharacteristics(void);
//tBleStatus BLE_DeviceInit(void);
//tBleStatus BLE_GetHardwareVersion(uint8_t *hwVersion, uint16_t *fwVersion);
//void BLE_SetDeviceConnectable(void);
//void BLE_AppUserEventRx(void *pData);
//
//
////TODO BLE characteristics update prototypes here
///*
//tBleStatus Environmental_Update(int32_t press, int16_t temp) {
//	tBleStatus ret;
//	uint8_t buff[8];
//	HOST_TO_LE_16(buff, HAL_GetTick() >> 3);
//
//	HOST_TO_LE_32(buff + 2, press);
//	HOST_TO_LE_16(buff + 6, temp);
//
//	ret = aci_gatt_update_char_value(HWServW2STHandle, EnvironmentalCharHandle,
//			0, 8, buff);
//
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("Error while updating TEMP characteristic: 0x%04X\r\n",ret) ;
//		return BLE_STATUS_ERROR;
//	}
//
//	return BLE_STATUS_SUCCESS;
//}
//*/
//
//#endif /* INC_BLE_APP_H_ */
