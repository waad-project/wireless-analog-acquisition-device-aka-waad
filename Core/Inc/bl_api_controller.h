/*
 * bl_api_controller.h
 *
 *  Created on: Oct 22, 2020
 *      Author: kaczp
 */

#ifndef INC_BL_API_CONTROLLER_H_
#define INC_BL_API_CONTROLLER_H_

#include "bl_rfcomm.h"

typedef enum {
	START_BYTE_OFFSET,
	SERVICE_BYTE_OFFSET,
	CHARACTERISTIC_BYTE_OFFSET,
	RW_BYTE_OFFSET,
	LENGTH_BYTE_OFFSET,
	REQUEST_DATA_BYTE_OFFSET=4,
	RESPONSE_DATA_BYTE_OFFSET,
	STOP_BYTE_OFFSET=5,
} Frame_Offsets;

typedef enum {
	_1_B,
	_2_B,
	_4_B,
	_8_B,
	_16_B,
	_32_B,
	_64_B,
	_128_B,
	_256_B
} DATA_LENGTH_BYTES;

const uint16_t DATA_LENGTH_BYTES_VALUES[9];

typedef enum {
	BATT_SRV=3,
	SD_SRV=1,
	ADC_SRV=2,
	CUSTOM_DATA_SRV=0x0F
} Services;

typedef enum {
	BATTERY_POWER_LEVEL,
	BATTERY_CHARGED
} BATTERY_Chars;

typedef enum {
	SDCARD_FLOAT_PRECISION,
	SDCARD_FORMAT,
	SDCARD_OVERFLOW,
	SDCARD_TIMESTAMP
} SDCARD_Chars;

typedef enum {
	ADC_RUNNING,
	ADC_PAUSE,
	ADC_DATA,
	ADC_FREQUENCY,
	ADC_FREQUENCY_AVAILABLE,
	ADC_CHANNELS_ACTIVE
} ADC_Chars;

typedef enum {
	CUSTOM_DATA_LENGTH
} CUSTOM_DATA_Chars;

typedef struct {
	BL_RfComm* blrf;
	osSemaphoreId_t executionBinarySemaphoreHandle;
} BL_API_Controller;

void BL_API_InitController(BL_API_Controller* blap, BL_RfComm* blrf, osSemaphoreId_t executionBinarySemaphoreHandle);
BL_API_Controller* BL_API_GetBL_API_Controller();
uint8_t BL_API_ValidateFrame(BL_API_Controller* blap);
void BL_API_DecodeRequest(BL_API_Controller* blap);

#endif /* INC_BL_API_CONTROLLER_H_ */
