/*
 * adc.h
 *
 *  Created on: Sep 14, 2020
 *      Author: Przemek
 */

#ifndef INC_ADC_H_
#define INC_ADC_H_

#include "stm32f4xx_hal.h"

#define ADC_DEFAULT_SAMPLE_TIME ADC_SAMPLETIME_144CYCLES

typedef struct {
	uint8_t active;
	ADC_ChannelConfTypeDef config;
} ADC_AdvChannelConfTypeDef;

/**
 * @brief	Predefined configurations for easier channel enabling/disabling
 */
typedef enum {
	ADC_CHAR_RUNNING,
	ADC_CHAR_PAUSE,
	ADC_CHAR_DATA,
	ADC_CHAR_FREQUENCY,
	ADC_CHAR_AVAILABLE_FREQUENCY,
	ADC_CHAR_ACTIVE_CHANNELS
} ADC_CharType;

#define ADC_OPERATING_CHANNELS 8
/**
 * @brief	Predefined configurations for easier channel enabling/disabling
 */
typedef enum {
	CH0 = ADC_CHANNEL_4,
	CH1 = ADC_CHANNEL_8,
	CH2 = ADC_CHANNEL_12,
	CH3 = ADC_CHANNEL_11,
	CH4 = ADC_CHANNEL_13,
	CH5 = ADC_CHANNEL_10,
	CH6 = ADC_CHANNEL_14,
	CH7 = ADC_CHANNEL_9
} ADC_ChannelPreConfig;

/**
 * @brief	Each value represent timer period to achieve specific sampling frequency
 */

#define ADC_FREQUENCIES_VARIANTS 4

const static uint32_t ADC_Frequencies[ADC_FREQUENCIES_VARIANTS] = {
	500, 1000, 2000, 4000,
};

const static uint32_t ADC_TimerPeriods[ADC_FREQUENCIES_VARIANTS] = {
	4000, 2000, 1000, 500,
};

/**
 * @brief	Each next two bytes truncates to 16bit code: XXY, which stands for value: XXY = XX * 10^Y
 */
#define ADC_FREQUENCY_CODE_SIZE 2
const static uint8_t ADC_TimerFrequenciesCodes[ADC_FREQUENCIES_VARIANTS*ADC_FREQUENCY_CODE_SIZE] = {
	5, 2, 10, 2, 20, 2, 40, 2
};

#define ADC_SAMPLES_PER_PACKAGE 128

typedef enum {
	_1_byte,
	_2_bytes,
	_4_bytes,
	_8_bytes,
	_16_bytes,
	_32_bytes,
	_64_bytes,
	_128_bytes,
	_256_bytes
} ADC_PackageSize;

const static uint16_t ADC_PackageSizePerActiveChannels[ADC_OPERATING_CHANNELS] = {
	1,
	2,
	4,
	8,
	16,
	32,
	64,
	128,
};

/**
 * @brief	Predefined indexes for ADC_TimerPeriods values
 */
typedef enum {
	_500_Hz,
	_1_kHz,
	_2_kHz,
	_4_kHz
} ADC_TimerFrequencyPreconfig;

/**
 * @brief	ADC smart live control structure definition
 */
typedef struct {
	uint16_t total_data_length;
	uint8_t adc_running;
	uint8_t adc_pause;
	uint8_t active_channels_count;
	uint8_t frequency;
	uint8_t float_resolution;
	uint16_t sample_buffer[ADC_SAMPLES_PER_PACKAGE];
	uint8_t sample_cnt;
	uint32_t frames_produced;
	uint32_t timer_int;
	ADC_HandleTypeDef *hadc;
	TIM_HandleTypeDef *htim;
	void (*ConversionCpltCallback)();
	ADC_InitTypeDef ADC_InitTemplate;
	uint8_t active_channels[ADC_OPERATING_CHANNELS];
	ADC_AdvChannelConfTypeDef channels[ADC_OPERATING_CHANNELS];
} ADC_Controller;

#define ADC_FLOAT_MIN_RESOLUTION 2
#define ADC_FLOAT_MAX_RESOLUTION 5

HAL_StatusTypeDef ADC_ControllerInit(ADC_Controller *adcc, ADC_HandleTypeDef *hadc, TIM_HandleTypeDef* htim);

uint8_t ADC_GetCntOfActiveChannels(ADC_Controller *adcc);
uint8_t ADC_GetFrequency(ADC_Controller *adcc);

HAL_StatusTypeDef ADC_UpdateSamplingFrequency(ADC_Controller *adcc, uint8_t frequency);
HAL_StatusTypeDef ADC_DisableChannel(ADC_Controller *adcc, ADC_AdvChannelConfTypeDef *channel);
HAL_StatusTypeDef ADC_EnableChannel(ADC_Controller *adcc, ADC_AdvChannelConfTypeDef *channel);
HAL_StatusTypeDef ADC_UpdateChannels(ADC_Controller *adcc, uint8_t *channels_active);
HAL_StatusTypeDef ADC_StartConversion(ADC_Controller *adcc);
HAL_StatusTypeDef ADC_EndConversion(ADC_Controller *adcc);
HAL_StatusTypeDef ADC_TogglePauseConversion(ADC_Controller *adcc, uint8_t on_off);

ADC_Controller* ADC_GetADCController();
ADC_HandleTypeDef* ADC_GetADCHandle();
TIM_HandleTypeDef* ADC_GetADCTimerHandle();

void ADC_CharModifiedCallback(ADC_CharType char_type, uint8_t *Attr_Data, uint16_t Attr_Data_Length);

void ADCInterruptCallback();

HAL_StatusTypeDef HAL_ADC_DeconfigChannel(ADC_HandleTypeDef *hadc,
		ADC_ChannelConfTypeDef *sConfig);
#endif /* INC_ADC_H_ */
