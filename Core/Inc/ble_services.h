///*
// * ble_services.h
// *
// *  Created on: Sep 23, 2020
// *      Author: Przemek
// */
//
//#ifndef INC_BLE_SERVICES_H_
//#define INC_BLE_SERVICES_H_
//
//#include "hci.h"
//
//#define NUMBER_OF_APPLICATION_SERVICES 3
//
//tBleStatus BLE_AddBatteryService(void);
//tBleStatus BLE_AddSDCardTimestampConfigurationService(void);
//tBleStatus BLE_AddADCConfigurationService(void);
//tBleStatus BLE_UpdateADC_Characteristics(void);
//tBleStatus BLE_UpdateBatteryCharacteristics(void);
//
//#endif /* INC_BLE_SERVICES_H_ */
