///*
// * sd_card.h
// *
// *  Created on: Oct 8, 2020
// *      Author: kaczp
// */
//
//#ifndef INC_SD_CARD_H_
//#define INC_SD_CARD_H_
//
//#include "integer.h"
//#include "stdbool.h"
//#include "stm32f4xx_hal.h"
//#include "fatfs.h"
//
//
////TODO: fix
//#define UNLOAD_1_ELEMENTS(arr) arr[0]
//#define UNLOAD_2_ELEMENTS(arr) arr[0], arr[1]
//#define UNLOAD_3_ELEMENTS(arr) arr[0], arr[1], arr[2]
//#define UNLOAD_4_ELEMENTS(arr) arr[0], arr[1], arr[2], arr[3]
//#define UNLOAD_5_ELEMENTS(arr) arr[0], arr[1], arr[2], arr[3], arr[4]
//#define UNLOAD_6_ELEMENTS(arr) arr[0], arr[1], arr[2], arr[3], arr[4], arr[5]
//#define UNLOAD_7_ELEMENTS(arr) arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]
//#define UNLOAD_8_ELEMENTS(arr) arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7]
//
//typedef struct {
//	RTC_TimeTypeDef sTime;
//	RTC_DateTypeDef sDate;
//	uint8_t format;
//	uint8_t file_opened_f;
//	uint8_t columns_count;
//	uint8_t timestamp[20];
//	UINT bw;
//} SD_CARD_Controller;
//
//SD_CARD_Controller *SDCard_GetSDCardController();
//void SDCard_PepareBufferForFileHeader(SD_CARD_Controller *sdcc);
//void SDCard_PepareBufferForSamplePackage(SD_CARD_Controller *sdcc);
//void SDCard_FillBufferWithSamplePackage();
//HAL_StatusTypeDef SDCard_Format(SD_CARD_Controller *sdcc);
//HAL_StatusTypeDef SDCard_SetTimestamp(SD_CARD_Controller *sdcc, uint8_t *data);
//HAL_StatusTypeDef SDCard_CreateNewMeasurementFile(SD_CARD_Controller *sdcc);
//HAL_StatusTypeDef SDCard_CloseMeasurementFile(SD_CARD_Controller *sdcc);
//void SDCard_WriteSamplesToFile(SD_CARD_Controller *sdcc);
//
//#endif /* INC_SD_CARD_H_ */
