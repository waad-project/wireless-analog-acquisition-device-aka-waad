/*
 * ble_rfcomm.h
 *
 *  Created on: Oct 21, 2020
 *      Author: kaczp
 */

#ifndef INC_BL_RFCOMM_H_
#define INC_BL_RFCOMM_H_

#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#define START 0xC0
#define STOP 0xFF

#define READ 0x00
#define WRITE 0x01

#define READ_BUFFER_SIZE 0x06

extern uint8_t CONFIRM_CONNECT_FRAME[6];

extern uint8_t BATTERY_SINGLE_DATA_BYTE_BUFFER[7];
extern uint8_t BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[8];

extern uint8_t SD_CARD_SINGLE_DATA_BYTE_BUFFER[7];

extern uint8_t ADC_SINGLE_DATA_BYTE_BUFFER[7];
extern uint8_t ADC_CHANNEL_DATA_BYTE_BUFFER[14];
extern uint8_t ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER[14];
extern uint8_t ADC_DATA_DATA_BYTE_BUFFER[262];

typedef struct {
	uint16_t unsuccessful_connections;

	//Connection flags
	volatile uint8_t con_f;
	volatile uint8_t pos_con_f;
	volatile uint8_t pos_lost_of_con_f;
	volatile uint8_t pending_con_f;

	//Common request flag
	volatile uint8_t pending_rq_f;
	uint8_t receive_buffer[6];

	//Long request flag
	volatile uint8_t long_data_f;
	volatile uint8_t pending_long_data_rq_f;
	uint8_t long_data_length;
	uint8_t long_data_receive_buffer[300];

	osThreadId_t blrfCommTaskHandle;
	osThreadId_t executionTask;
	UART_HandleTypeDef* huart;
} BL_RfComm;

void BL_RFCOMM_Init(BL_RfComm* blrf, UART_HandleTypeDef* huart, osThreadId_t blrfCommTaskHandle, osThreadId_t executionTask);
uint8_t BL_RFCOMM_CheckIfLostConnection(BL_RfComm* blrf);
void BL_RFCOMM_GetRequest(BL_RfComm* blrf);
void BL_RFCOMM_Response(BL_RfComm* blrf, uint8_t* data, uint16_t length, uint8_t clearPendingFlag);
void BL_RFCOMM_WaitUntilUART_Ready(BL_RfComm* blrf,uint32_t Interval);
void BL_RFCOMM_WaitUntilUART_TX_Ready(BL_RfComm* blrf,uint32_t Interval);
void BL_RFCOMM_WaitUntilUART_RX_Ready(BL_RfComm* blrf,uint32_t Interval);
uint8_t BL_RFCOMM_Connected(BL_RfComm* blrf);
void BL_RFCOMM_ResetFlags(BL_RfComm* blrf);
uint8_t BL_RFCOMM_WaitUntilConnectedState(BL_RfComm* blrf, uint32_t Interval, uint32_t total_timeout);
void BL_RFCOMM_TryToConnect(BL_RfComm* blrf,uint32_t Interval, uint32_t total_timeout);
void BL_RFCOMM_WaitUntilConnectionConfirmed(BL_RfComm* blrf,uint32_t Interval, uint32_t total_timeout);
BL_RfComm* BL_RFCOMM_GetBLRfComm();
UART_HandleTypeDef* BL_RFCOMM_GetUARTHandle();
void BL_RFCOMM_ConnectionChangedStateCallback();
void BL_RFCOMM_RequestReceivedCallback();
void BL_RFCOMM_PossibleConnectionConfirmCallback();

#endif /* INC_BL_RFCOMM_H_ */
