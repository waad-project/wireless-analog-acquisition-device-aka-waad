///*
// * ble_callbacks.c
// *
// *  Created on: Sep 26, 2020
// *      Author: Przemek
// */
//
///* Includes */
//#include "ble_app.h"
//#include "bluenrg1_events.h"
//#include "bluenrg1_gatt_aci.h"
//#include "ble_services.h"
//#include "adc.h"
//
///* Exported variables */
//extern volatile uint8_t set_connectable;
//extern volatile uint16_t connection_handle;
//extern volatile uint32_t connected;
//extern uint16_t BATTERY_ServiceHandle, BATTERY_Level_CharHandle,
//		BATTERY_PowerState_CharHandle;
//extern uint16_t SDCard_ServiceHandle, SDCardTimestamp_CharHandle,
//		SDCardFormat_CharHandle, SDCardFormatConfirmation_CharHandle;
//extern uint16_t ADC_ServiceHandle, ADC_Data_CharHandle, ADC_FrequenciesAvailable_CharHandle,
//		ADC_Frequency_CharHandle, ADC_ActiveChannels_CharHandle, ADC_Running_CharHandle,
//		ADC_Pause_CharHandle, ADC_SDCardFloatResolution_CharHandle;
//
//
///* ***************** BlueNRG-1 Stack Callbacks ********************************/
///**
// * @brief  This event indicates that a new connection has been created
// *
// * @param  See file bluenrg1_events.h
// * @retval See file bluenrg1_events.h
// */
//void hci_le_connection_complete_event(uint8_t Status,
//		uint16_t Connection_Handle, uint8_t Role, uint8_t Peer_Address_Type,
//		uint8_t Peer_Address[6], uint16_t Conn_Interval, uint16_t Conn_Latency,
//		uint16_t Supervision_Timeout, uint8_t Master_Clock_Accuracy) {
//	connected = TRUE;
//	connection_handle = Connection_Handle;
//
//	BSP_LED_Off(LED2); //activity led
//}
//
///**
// * @brief  This event occurs when a connection is terminated
// *
// * @param  See file bluenrg1_events.h
// * @retval See file bluenrg1_events.h
// */
//void hci_disconnection_complete_event(uint8_t Status,
//		uint16_t Connection_Handle, uint8_t Reason) {
//	connected = FALSE;
//	/* Make the device connectable again */
//	set_connectable = TRUE;
//	connection_handle = 0;
//	PRINT_DBG("Disconnected\r\n");
//
//	BSP_LED_On(LED2); //activity led
//}
//
///**
// * @brief  This event is given when a read request is received
// *         by the server from the client
// * @param  See file bluenrg1_events.h
// * @retval See file bluenrg1_events.h
// */
//void aci_gatt_attribute_modified_event(uint16_t Connection_Handle,
//                                       uint16_t Attr_Handle,
//                                       uint16_t Offset,
//                                       uint16_t Attr_Data_Length,
//                                       uint8_t Attr_Data[]) {
//	//TODO: For each GATT characteristic there should be callback if it can be overwritten by client
////	if(Attr_Handle == SDCardTimestamp_CharHandle) SDCard_ModifiedCallback();
////	if(Attr_Handle == SDCardFormat_CharHandle) SDCard_ModifiedCallback();
////	if(Attr_Handle == SDCardFormatConfirmation_CharHandle) SDCard_ModifiedCallback();
//
//	if(Attr_Handle == ADC_Frequency_CharHandle+1) ADC_CharModifiedCallback(ADC_CHAR_FREQUENCY, Attr_Data, Attr_Data_Length);
//	if(Attr_Handle == ADC_ActiveChannels_CharHandle+1) ADC_CharModifiedCallback(ADC_CHAR_ACTIVE_CHANNELS, Attr_Data, Attr_Data_Length);
//	if(Attr_Handle == ADC_Running_CharHandle+1) ADC_CharModifiedCallback(ADC_CHAR_RUNNING, Attr_Data, Attr_Data_Length);
//	if(Attr_Handle == ADC_Pause_CharHandle+1) ADC_CharModifiedCallback(ADC_CHAR_PAUSE, Attr_Data, Attr_Data_Length);
//	if(Attr_Handle == ADC_SDCardFloatResolution_CharHandle+1) ADC_CharModifiedCallback(ADC_CHAR_SD_CARD_FLOAT_RESOLUTION, Attr_Data, Attr_Data_Length);
//
////	if (connection_handle != 0) {
////		aci_gatt_co
////		ret = aci_gatt_allow_read(connection_handle);
////		if (ret != BLE_STATUS_SUCCESS) {
////			PRINT_DBG("aci_gatt_allow_read() failed: 0x%02x\r\n", ret);
////		}
////	}
//}
//
//extern uint16_t SDCard_ServiceHandle, SDCardTimestamp_CharHandle,
//		SDCardFormat_CharHandle, SDCardFormatConfirmation_CharHandle;
//extern uint16_t ADC_ServiceHandle, ADC_Data_CharHandle, ADC_Frequency_CharHandle,
//		ADC_ActiveChannels_CharHandle, ADC_Running_CharHandle,
//		ADC_Pause_CharHandle, ADC_SDCardFloatResolution_CharHandle;
//
//void SDCard_ModifiedCallback(uint16_t Attr_Data_Length, uint8_t Attr_Data[]) {
//
//}
//
//void ADC_ModifiedCallback(uint16_t Attr_Data_Length, uint8_t Attr_Data[]) {
//
//}
//
//void aci_gatt_read_permit_req_event(uint16_t Connection_Handle,
//                                    uint16_t Attribute_Handle,
//                                    uint16_t Offset) {
//	tBleStatus ret;
//	if (connection_handle != 0) {
//		ret = aci_gatt_allow_read(connection_handle);
//		if (ret != BLE_STATUS_SUCCESS) {
//			PRINT_DBG("aci_gatt_allow_read() failed: 0x%02x\r\n", ret);
//		}
//	}
//}
////
////void Read_Request_CB(uint16_t handle) {
////	tBleStatus ret;
////
////	if (handle == AccGyroMagCharHandle + 1) {
////		Acc_Update(&x_axes, &g_axes, &m_axes);
////	} else if (handle == EnvironmentalCharHandle + 1) {
////		float data_t, data_p;
////		data_t = 27.0 + ((uint64_t) rand() * 5) / RAND_MAX; //T sensor emulation
////		data_p = 1000.0 + ((uint64_t) rand() * 100) / RAND_MAX; //P sensor emulation
////		Environmental_Update((int32_t) (data_p * 100), (int16_t) (data_t * 10));
////	}
////
////	if (connection_handle != 0) {
////		ret = aci_gatt_allow_read(connection_handle);
////		if (ret != BLE_STATUS_SUCCESS) {
////			PRINT_DBG("aci_gatt_allow_read() failed: 0x%02x\r\n", ret);
////		}
////	}
////}
////
