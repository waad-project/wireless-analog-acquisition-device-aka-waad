///*
// * ble_app.c
// *
// *  Created on: Sep 26, 2020
// *      Author: Przemek
// */
//
///* Includes */
//#include "ble_app.h"
//#include "ble_services.h"
//#include <stdlib.h>
//#include "bluenrg1_aci.h"
//#include "bluenrg1_hci_le.h"
//#include "bluenrg1_events.h"
//#include "hci_tl.h"
//#include "bluenrg_utils.h"
//#include "custom.h"
//
///* Private variables */
//volatile uint8_t notification_enabled = FALSE;
//volatile uint8_t connected = FALSE;
//volatile uint8_t set_connectable = 1;
//volatile uint16_t connection_handle = 0;
//volatile uint8_t request_free_fall_notify = FALSE;
//
///* Private variables */
//uint8_t bdaddr[BDADDR_SIZE];
//
///*
// * @brief BlueNRG-2 background task
// * @param  None
// * @retval None
// */
//void BLE_AppProcess(void) {
//	PRINT_DBG("BlueNRG-2 Processing.\r\n");
//	hci_user_evt_proc();
//	BLE_MainTask();
//}
//
//void BLE_GenerateDeviceName(uint8_t *device_name, uint8_t *mac_address,
//		uint8_t size) {
////	memcpy(device_name, SENSOR)
//}
//
///**
// * @brief  Set_DeviceConnectable
// * @note   Puts the device in connectable mode
// * @param  None
// * @retval None
// */
//void BLE_SetDeviceConnectable(void) {
//
//	uint8_t local_name[] = { AD_TYPE_COMPLETE_LOCAL_NAME, WAAD_SHORT_NAME };
//
//	// https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile/
//	// AD structure: 1-byte length, 1-byte AD type, n-bytes AD data
//	uint8_t manuf_data[54] =
//			{ 2 /* length: AD type 1b + Transmit Power Level 1b */,
//					0x0A /* Tx Power Level */, 0x00 /* 0 dBm */,
//
//					5 /* length: AD type 1b + short local name 4b */,
//					0x08 /* Short local name */,
//					WAAD_SHORT_NAME,
//
//					35 /* length: AD type 1b + complete local name 34b */, 0x09, /* Complete local name */
//					WAAD_COMPLETE_NAME,
//
////			0x03, 0x00, 0xF4, /* ACC+Gyro+Mag 0xE0 | 0x04 Temp | 0x10 Pressure */
//
//					8 /* length: AD type 1b + Device address type 1b + Device address type 6b */,
//					0x1B /* LE Bluetooth device address */,
//					0x00 /* 0 - Public device address, 1 - Random device address*/,
//					bdaddr[5], bdaddr[4], bdaddr[3], bdaddr[2], bdaddr[1],
//					bdaddr[0] /*Device address LSB first MSB last*/
//			};
//
//	if (hci_le_set_scan_response_data(0, NULL) != BLE_STATUS_SUCCESS)
//		PRINT_DBG("hci_le_set_scan_response_data() failed: 0x%02x\r\n", ret);
//	else
//		PRINT_DBG("hci_le_set_scan_response_data() --> SUCCESS\r\n");
//
//	PRINT_DBG("Set General Discoverable Mode.\r\n");
//
//	if (aci_gap_set_discoverable(ADV_DATA_TYPE, ADV_INTERV_MIN,
//	ADV_INTERV_MAX, STATIC_RANDOM_ADDR,
//	NO_WHITE_LIST_USE, sizeof(local_name), local_name, 0, NULL, 0,
//			0) != BLE_STATUS_SUCCESS)
//		PRINT_DBG("aci_gap_set_discoverable() failed: 0x%02x\r\n", ret);
//	else
//		PRINT_DBG("aci_gap_set_discoverable() --> SUCCESS\r\n");
//
//	PRINT_DBG("Set advertisement data.\r\n");
//
//	if (aci_gap_update_adv_data(54, manuf_data) != BLE_STATUS_SUCCESS)
//		PRINT_DBG("aci_gap_update_adv_data() failed: 0x%02x\r\n", ret);
//	else
//		PRINT_DBG("aci_gap_update_adv_data() --> SUCCESS\r\n");
//}
//
///**
// * @brief  User Process
// *
// * @param  None
// * @retval None
// */
//void BLE_MainTask(void) {
//	/* Make the device discoverable */
//	if (set_connectable) {
//		BLE_SetDeviceConnectable();
//		set_connectable = FALSE;
//	}
//
//	BSP_LED_Toggle(LED2);
//
//	if (connected) {
//		//TODO: Here main app operating
//		HAL_Delay(1000); /* wait 1 sec before sending new data */
//	}
//}
//
///**
// * @brief  Initialize the device sensors
// *
// * @param  None
// * @retval None
// */
//tBleStatus BLE_AppInit(void) {
//	HAL_NVIC_DisableIRQ(EXTI0_IRQn);
//	tBleStatus ret;
//	BSP_LED_Init(LED2);
//	BSP_COM_Init(COM1);
//	hci_init(BLE_AppUserEventRx, NULL);
//
//	PRINT_DBG("BlueNRG-2 SensorDemo_BLESensor-App Application\r\n");
//
//	ret = BLE_DeviceInit();
//	/* Init Sensor Device */
//	if (ret != BLE_STATUS_SUCCESS) {
//		BSP_LED_On(LED2);
//		PRINT_DBG("SensorDeviceInit()--> Failed 0x%02x\r\n", ret);
//	}PRINT_DBG("BLE Stack Initialized & Device Configured\r\n");
//	return ret;
//}
//
///**
// * @brief Add all device services with characteristics
// *
// * @param  None
// * @retval tBleStatus
// */
//tBleStatus BLE_AddServices(void) {
//	tBleStatus ret;
//	ret = BLE_AddBatteryService();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 Battery service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 Battery service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//	ret = BLE_AddSDCardTimestampConfigurationService();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 SDCard service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 SDCard service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//	ret = BLE_AddADCConfigurationService();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 ADC service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 ADC service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//	return BLE_STATUS_SUCCESS;
//}
//
//tBleStatus BLE_UpdateCharacteristics(void) {
//	tBleStatus ret;
//	ret = BLE_UpdateADC_Characteristics();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 Battery service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 Battery service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//	ret = BLE_UpdateBatteryCharacteristics();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 Battery service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 Battery service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//
//	ret = BLE_AddADCConfigurationService();
//	if (ret == BLE_STATUS_SUCCESS) {
//		PRINT_DBG("BlueNRG2 ADC service added successfully.\r\n");
//	} else {
//		PRINT_DBG("Error while adding BlueNRG2 ADC service: 0x%02x\r\n", ret);
//		return ret;
//	}
//
//	return BLE_STATUS_SUCCESS;
//}
//
///**
// * @brief Initialize the device hardware, gap central role, ble transmission power level
// *
// * @param  None
// * @retval tBleStatus,
// */
//tBleStatus BLE_DeviceInit(void) {
//	tBleStatus ret;
//	uint16_t service_handle, dev_name_char_handle, appearance_char_handle;
//	uint8_t device_name[] = { WAAD_SHORT_NAME };
//	uint8_t hwVersion;
//	uint16_t fwVersion;
//	uint8_t bdaddr_len_out;
//	uint8_t config_data_stored_static_random_address = 0x80; /* Offset of the static random address stored in NVM */
//
//	/* Sw reset of the device */
//	hci_reset();
//	/**
//	 *  To support both the BlueNRG-2 and the BlueNRG-2N a minimum delay of 2000ms is required at device boot
//	 */
//	HAL_Delay(2000);
//
//	/* get the BlueNRG HW and FW versions */
//	BLE_GetHardwareVersion(&hwVersion, &fwVersion);
//
//	PRINT_DBG("HWver %d\nFWver %d\r\n", hwVersion, fwVersion);
//
//	ret = aci_hal_read_config_data(config_data_stored_static_random_address,
//			&bdaddr_len_out, bdaddr);
//
//	if (ret) {
//		PRINT_DBG("Read Static Random address failed.\r\n");
//	}
//
//	if ((bdaddr[5] & 0xC0) != 0xC0) {
//		PRINT_DBG("Static Random address not well formed.\r\n");
//		while (1)
//			;
//	}
//
//	/* Set the TX power -2 dBm */
//	aci_hal_set_tx_power_level(1, 4);
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("Error in aci_hal_set_tx_power_level() 0x%04x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("aci_hal_set_tx_power_level() --> SUCCESS\r\n");
//	}
//
//	/* GATT Init */
//	ret = aci_gatt_init();
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("aci_gatt_init() failed: 0x%02x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("aci_gatt_init() --> SUCCESS\r\n");
//	}
//
//	/* GAP Init */
//	ret = aci_gap_init(GAP_PERIPHERAL_ROLE, 0, 0x07, &service_handle,
//			&dev_name_char_handle, &appearance_char_handle);
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("aci_gap_init() failed: 0x%02x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("aci_gap_init() --> SUCCESS\r\n");
//	}
//
//	/* Update device name */
//	ret = aci_gatt_update_char_value(service_handle, dev_name_char_handle, 0,
//			sizeof(device_name), device_name);
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("aci_gatt_update_char_value() failed: 0x%02x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("aci_gatt_update_char_value() --> SUCCESS\r\n");
//	}
//
//	/* BLE Security v4.2 is supported: BLE stack FW version >= 2.x (new API prototype) */
//	ret = aci_gap_set_authentication_requirement(BONDING,
//	MITM_PROTECTION_REQUIRED,
//	SC_IS_SUPPORTED,
//	KEYPRESS_IS_NOT_SUPPORTED, 7, 16,
//	USE_FIXED_PIN_FOR_PAIRING, 123456, 0x00);
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("aci_gap_set_authentication_requirement()failed: 0x%02x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("aci_gap_set_authentication_requirement() --> SUCCESS\r\n");
//	}
//
//	PRINT_DBG("BLE Stack Initialized with SUCCESS\r\n");
//
//	ret = BLE_AddServices();
//	if (ret != BLE_STATUS_SUCCESS) {
//		PRINT_DBG("Failed to configure services failed: 0x%02x\r\n", ret);
//		return ret;
//	} else {
//		PRINT_DBG("BLE_AddServices() --> SUCCESS\r\n");
//	}
//
//
//	return BLE_STATUS_SUCCESS;
//}
//
///**
// * @brief Gets BLE hardware and firmware version
// *
// * @param  Hardware version
// * @param  Firmware version
// * @retval Status
// */
//tBleStatus BLE_GetHardwareVersion(uint8_t *hwVersion, uint16_t *fwVersion) {
//	uint8_t status;
//	uint8_t hci_version, lmp_pal_version;
//	uint16_t hci_revision, manufacturer_name, lmp_pal_subversion;
//
//	status = hci_read_local_version_information(&hci_version, &hci_revision,
//			&lmp_pal_version, &manufacturer_name, &lmp_pal_subversion);
//
//	if (status == BLE_STATUS_SUCCESS) {
//		*hwVersion = hci_revision >> 8;
//		*fwVersion = (hci_revision & 0xFF) << 8;         // Major Version Number
//		*fwVersion |= ((lmp_pal_subversion >> 4) & 0xF) << 4; // Minor Version Number
//		*fwVersion |= lmp_pal_subversion & 0xF;          // Patch Version Number
//	}
//	return status;
//}
//
///**
// * @brief Handling all important BLE api (hci, gap, gatt) events
// *
// * @param  Hardware version
// * @retval None
// */
//void BLE_AppUserEventRx(void *pData) {
//	uint32_t i;
//
//	hci_spi_pckt *hci_pckt = (hci_spi_pckt*) pData;
//
//	if (hci_pckt->type == HCI_EVENT_PKT) {
//		hci_event_pckt *event_pckt = (hci_event_pckt*) hci_pckt->data;
//
//		if (event_pckt->evt == EVT_LE_META_EVENT) {
//			evt_le_meta_event *evt = (void*) event_pckt->data;
//
//			for (i = 0;
//					i
//							< (sizeof(hci_le_meta_events_table)
//									/ sizeof(hci_le_meta_events_table_type));
//					i++) {
//				if (evt->subevent == hci_le_meta_events_table[i].evt_code) {
//					hci_le_meta_events_table[i].process((void*) evt->data);
//				}
//			}
//		} else if (event_pckt->evt == EVT_VENDOR) {
//			evt_blue_aci *blue_evt = (void*) event_pckt->data;
//
//			for (i = 0;
//					i
//							< (sizeof(hci_vendor_specific_events_table)
//									/ sizeof(hci_vendor_specific_events_table_type));
//					i++) {
//				if (blue_evt->ecode
//						== hci_vendor_specific_events_table[i].evt_code) {
//					hci_vendor_specific_events_table[i].process(
//							(void*) blue_evt->data);
//				}
//			}
//		} else {
//			for (i = 0;
//					i
//							< (sizeof(hci_events_table)
//									/ sizeof(hci_events_table_type)); i++) {
//				if (event_pckt->evt == hci_events_table[i].evt_code) {
//					hci_events_table[i].process((void*) event_pckt->data);
//				}
//			}
//		}
//	}
//}
