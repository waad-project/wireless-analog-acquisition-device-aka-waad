///*
// * ble_services.c
// *
// *  Created on: Sep 23, 2020
// *      Author: Przemek
// */
//
///* Includes */
//#include <stdlib.h>
//#include "ble_services.h"
//#include "bluenrg1_aci.h"
//#include "bluenrg1_hci_le.h"
//#include "bluenrg1_gatt_aci.h"
//#include "adc.h"
//#include "battery.h"
//
//const static uint8_t UUID_ARR[15][16] = {
//	/* Battery service, indexes: 0, 1, 2 */
//	{0x00,0x00,0x18,0x0F,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5F,0x9B,0x34,0xFB},
//	{0x00,0x00,0x2A,0x19,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5F,0x9B,0x34,0xFB},
//	{0x00,0x00,0x2A,0x1A,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5F,0x9B,0x34,0xFB},
//
//	/* SDCard service, indexes: 3, 4, 5, 6 */
//	{0x0E,0x23,0x6A,0xCC,0x39,0xAF,0x47,0x8F,0x98,0x13,0x9D,0xA7,0xEF,0x26,0x67,0x52},
//	{0x00,0x00,0x2A,0x0A,0x00,0x00,0x10,0x00,0x80,0x00,0x00,0x80,0x5F,0x9B,0x34,0xFB},
//	{0x0E,0x23,0x6A,0xCC,0x39,0xB0,0x47,0x8F,0x98,0x13,0x9D,0xA7,0xEF,0x26,0x67,0x52},
//	{0x0E,0x23,0x6A,0xCC,0x39,0xB1,0x47,0x8F,0x98,0x13,0x9D,0xA7,0xEF,0x26,0x67,0x52},
//
//	/* ADC service, indexes: 7, 8, 9, 10, 11, 12, 13, 24 */
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC0,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC1,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC2,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88}, //available fequencies
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC3,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC4,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC5,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC6,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//	{0x64,0x29,0x0B,0x11,0x36,0x55,0x4E,0xC7,0xA2,0xB0,0xB2,0x6E,0x52,0x24,0x49,0x88},
//};
//
///* UUIDS */
//#define BLE_ADD_BATTERY_SERVICE_UUID UUID_ARR[0]
//#define BLE_ADD_BATTERY_LEVEL_CHAR_UUID UUID_ARR[1]
//#define BLE_ADD_BATTERY_POWER_STATE_CHAR_UUID UUID_ARR[2]
//
//#define BLE_ADD_SD_CARD_SERVICE_UUID UUID_ARR[3]
//#define BLE_ADD_SD_CARD_TIMESTAMP_CHAR_UUID UUID_ARR[4]
//#define BLE_ADD_SD_CARD_FORMAT_CHAR_UUID UUID_ARR[5]
//#define BLE_ADD_SD_CARD_FORMAT_CONFIRMATION_CHAR_UUID UUID_ARR[6]
//
//#define BLE_ADC_SERVICE_UUID UUID_ARR[7]
//#define BLE_ADC_DATA_CHAR_UUID UUID_ARR[8]
//#define BLE_ADC_FREQUENCIES_AVAILABLE_CHAR_UUID UUID_ARR[9]
//#define BLE_ADC_FREQUENCY_CHAR_UUID UUID_ARR[10]
//#define BLE_ADC_ACTIVE_CHANNELS_CHAR_UUID UUID_ARR[11]
//#define BLE_ADC_RUNNING_CHAR_UUID UUID_ARR[12]
//#define BLE_ADC_PAUSE_CHAR_UUID UUID_ARR[13]
//#define BLE_ADC_SD_CARD_FLOAT_RESOLUTION_CHAR_UUID UUID_ARR[14]
//
//
///* Private variables ---------------------------------------------------------*/
//Service_UUID_t service_uuid;
//Char_UUID_t char_uuid;
//uint16_t BATTERY_ServiceHandle, BATTERY_Level_CharHandle,
//		BATTERY_PowerState_CharHandle;
//uint16_t SDCard_ServiceHandle, SDCardTimestamp_CharHandle,
//		SDCardFormat_CharHandle, SDCardFormatConfirmation_CharHandle;
//uint16_t ADC_ServiceHandle, ADC_Data_CharHandle, ADC_Frequency_CharHandle, ADC_FrequenciesAvailable_CharHandle,
//		ADC_ActiveChannels_CharHandle, ADC_Running_CharHandle,
//		ADC_Pause_CharHandle, ADC_SDCardFloatResolution_CharHandle;
//
//void swap_bytes(uint8_t* x, uint8_t* y) {
//	uint8_t tmp= *x;
//	*x=*y;
//	*y=tmp;
//}
//
//void reverse_array_of_bytes(uint8_t* s_arr, uint16_t n) {
//	uint8_t i;
//	for(i=0; i<n/2; i++) swap_bytes(&s_arr[i], &s_arr[n-i-1]);
//}
//
///**
// * @brief Add the control service for configuring TimeStamp and controlling SDCard
// * @param None
// * @retval tBleStatus Status
// * @characteristics Battery_Level, Battery_Power_State
// */
//tBleStatus BLE_AddBatteryService(void) {
//	tBleStatus ret;
//
//	/* num of characteristics of this service */
//	uint8_t char_number = 2;
//
//	/* number of attribute records that can be added to this service */
//	uint8_t max_attribute_records = 1 + (3 * char_number);
//
//	memcpy(&service_uuid.Service_UUID_128, BLE_ADD_BATTERY_SERVICE_UUID, 16);
//	reverse_array_of_bytes(&service_uuid.Service_UUID_128, 16);
//	ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE,
//			max_attribute_records, &BATTERY_ServiceHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADD_BATTERY_LEVEL_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(BATTERY_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//	CHAR_PROP_NOTIFY | CHAR_PROP_READ,
//	ATTR_PERMISSION_NONE,
//	GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&BATTERY_Level_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADD_BATTERY_POWER_STATE_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(BATTERY_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//	CHAR_PROP_NOTIFY | CHAR_PROP_READ,
//	ATTR_PERMISSION_NONE,
//	GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&BATTERY_PowerState_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	return BLE_STATUS_SUCCESS;
//}
//
///**
// * @brief Add the control service for configuring TimeStamp and controlling SDCard
// * @param None
// * @retval tBleStatus Status
// * @characteristics Day_Date_Time, SD_Card_Format_Flag, Sd_Card_Format_Confirmation_Flag
// */
//tBleStatus BLE_AddSDCardTimestampConfigurationService(void) {
//	tBleStatus ret;
//
//	/* num of characteristics of this service */
//	uint8_t char_number = 3;
//
//	/* number of attribute records that can be added to this service */
//	uint8_t max_attribute_records = 1 + (3 * char_number);
//
//	/* add HW_SENS_W2ST service */
//	memcpy(&service_uuid.Service_UUID_128, BLE_ADD_SD_CARD_SERVICE_UUID, 16);
//	reverse_array_of_bytes(&service_uuid.Service_UUID_128, 16);
//	ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE,
//			max_attribute_records, &SDCard_ServiceHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* add HW_SENS_W2ST service */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADD_SD_CARD_TIMESTAMP_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(SDCard_ServiceHandle, UUID_TYPE_128,
//			&char_uuid, 8,
//			CHAR_PROP_NOTIFY | CHAR_PROP_READ | CHAR_PROP_WRITE
//					| CHAR_PROP_SIGNED_WRITE | CHAR_PROP_WRITE_WITHOUT_RESP,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP
//					| GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&SDCardTimestamp_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* Fill the Environmental BLE Characteristc */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADD_SD_CARD_FORMAT_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(SDCard_ServiceHandle, UUID_TYPE_128,
//			&char_uuid, 1,
//			CHAR_PROP_WRITE | CHAR_PROP_SIGNED_WRITE
//					| CHAR_PROP_WRITE_WITHOUT_RESP,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&SDCardFormat_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* Fill the AccGyroMag BLE Characteristc */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADD_SD_CARD_FORMAT_CONFIRMATION_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(SDCard_ServiceHandle, UUID_TYPE_128,
//			&char_uuid, 1,
//			CHAR_PROP_WRITE | CHAR_PROP_SIGNED_WRITE
//					| CHAR_PROP_WRITE_WITHOUT_RESP,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_WRITE_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&SDCardFormatConfirmation_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	return BLE_STATUS_SUCCESS;
//}
//
///**
// * @brief Add the control service for configuring ADC controller
// * @param None
// * @retval tBleStatus Status
// * @characteristics Frequency, ADC_Active_Channels, Running_Flag, Pause_Flag, ADC_SD_Card_Float_Resolution
// */
//tBleStatus BLE_AddADCConfigurationService(void) {
//	tBleStatus ret;
//
//	/* num of characteristics of this service */
//	uint8_t char_number = 7+5;
//
//	/* number of attribute records that can be added to this service */
//	uint8_t max_attribute_records = 1 + (3 * char_number);
//
//	/* add HW_SENS_W2ST service */
//	memcpy(&service_uuid.Service_UUID_128, BLE_ADC_SERVICE_UUID, 16);
//	reverse_array_of_bytes(&service_uuid.Service_UUID_128, 16);
//	ret = aci_gatt_add_service(UUID_TYPE_128, &service_uuid, PRIMARY_SERVICE,
//			max_attribute_records, &ADC_ServiceHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_DATA_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 16,
//			CHAR_PROP_NOTIFY | CHAR_PROP_READ,
//			ATTR_PERMISSION_NONE,
//			GATT_DONT_NOTIFY_EVENTS, 16, 1, &ADC_Data_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_FREQUENCIES_AVAILABLE_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, ADC_FREQUENCIES_VARIANTS*ADC_FREQUENCY_CODE_SIZE,
//			CHAR_PROP_READ,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP, 16, 0,
//			&ADC_FrequenciesAvailable_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_FREQUENCY_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//			CHAR_PROP_READ | CHAR_PROP_WRITE,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0,
//			&ADC_Frequency_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_ACTIVE_CHANNELS_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 8,
//			CHAR_PROP_READ | CHAR_PROP_WRITE,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0,
//			&ADC_ActiveChannels_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* Fill the AccGyroMag BLE Characteristc */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_RUNNING_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//			CHAR_PROP_READ | CHAR_PROP_WRITE,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0,
//			&ADC_Running_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* Fill the Environmental BLE Characteristc */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_PAUSE_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//			CHAR_PROP_READ | CHAR_PROP_WRITE,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0,
//			&ADC_Pause_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	/* Fill the AccGyroMag BLE Characteristc */
//	memcpy(&char_uuid.Char_UUID_128, BLE_ADC_SD_CARD_FLOAT_RESOLUTION_CHAR_UUID, 16);
//	reverse_array_of_bytes(&char_uuid.Char_UUID_128, 16);
//	ret = aci_gatt_add_char(ADC_ServiceHandle, UUID_TYPE_128, &char_uuid, 1,
//			CHAR_PROP_READ | CHAR_PROP_WRITE,
//			ATTR_PERMISSION_NONE,
//			GATT_NOTIFY_READ_REQ_AND_WAIT_FOR_APPL_RESP | GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0,
//			&ADC_SDCardFloatResolution_CharHandle);
//	if (ret != BLE_STATUS_SUCCESS)
//		return BLE_STATUS_ERROR;
//
//	return BLE_STATUS_SUCCESS;
//}
//
//tBleStatus BLE_UpdateADC_Characteristics(void) {
//	tBleStatus ret;
//	ADC_Controller *adcc = ADC_GetADCController();
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_FrequenciesAvailable_CharHandle,
//		0, ADC_FREQUENCIES_VARIANTS*ADC_FREQUENCY_CODE_SIZE, ADC_TimerFrequenciesCodes);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_Frequency_CharHandle,
//		0, 1, adcc->frequency);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_ActiveChannels_CharHandle,
//		0, ADC_OPERATING_CHANNELS, adcc->active_channels);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_Pause_CharHandle,
//			0, 1, adcc->adc_pause);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_Running_CharHandle,
//			0, 1, adcc->adc_running);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	ret = aci_gatt_update_char_value(ADC_ServiceHandle, ADC_SDCardFloatResolution_CharHandle,
//			0, 1, adcc->float_resolution);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	return BLE_STATUS_SUCCESS;
//}
//
//
//tBleStatus BLE_UpdateBatteryCharacteristics(void) {
//	tBleStatus ret;
//	BATTERY_Controller *batc = BATTERY_GetBatteryController();
//
//	ret = aci_gatt_update_char_value(BATTERY_ServiceHandle, BATTERY_Level_CharHandle,
//		0, ADC_FREQUENCIES_VARIANTS*ADC_FREQUENCY_CODE_SIZE, ADC_TimerFrequenciesCodes);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	ret = aci_gatt_update_char_value(BATTERY_ServiceHandle, BATTERY_PowerState_CharHandle,
//		0, 1, ADC_TimerFrequenciesCodes);
//	if (ret != BLE_STATUS_SUCCESS){
//	    PRINT_DBG("Error while updating Sensor Fusion characteristic: 0x%02X\r\n",ret) ;
//	    return BLE_STATUS_ERROR;
//	}
//
//	//TODO: Implement updating all necessary ADC characteristics here
//	return BLE_STATUS_SUCCESS;
//}
