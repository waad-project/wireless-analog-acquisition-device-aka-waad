/*
 * adc.c
 *
 *  Created on: Sep 14, 2020
 *      Author: Przemek
 */

#include "adc.h"
#include "string.h"
#include "bl_api_controller.h"
#include "sd_card.h"

ADC_Controller adcc;

HAL_StatusTypeDef ADC_ControllerInit(ADC_Controller *adcc, ADC_HandleTypeDef *hadc, TIM_HandleTypeDef* htim) {
	adcc->hadc=hadc;
	adcc->htim=htim;
	adcc->float_resolution=2;
	adcc->hadc->Instance = ADC1;
	adcc->frames_produced=0;
	adcc->active_channels_count=8;
	uint8_t active_channels[ADC_OPERATING_CHANNELS] = { 1, 1, 1, 1, 1, 1, 1, 1};

	ADC_AdvChannelConfTypeDef channels[ADC_OPERATING_CHANNELS] = {
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH0, 1, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH1, 2, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH2, 3, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH3, 4, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH4, 5, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH5, 6, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH6, 7, ADC_DEFAULT_SAMPLE_TIME}},
		{.active=1, .config = (ADC_ChannelConfTypeDef){CH7, 1, ADC_DEFAULT_SAMPLE_TIME}}
	};

	ADC_InitTypeDef init = {
		.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2,
		.Resolution = ADC_RESOLUTION_12B,
		.ScanConvMode = ENABLE,
		.ContinuousConvMode = DISABLE,
		.DiscontinuousConvMode = DISABLE,
		.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_RISING,
		.ExternalTrigConv = ADC_EXTERNALTRIGCONV_T2_TRGO,
		.DataAlign = ADC_DATAALIGN_RIGHT,
		.NbrOfConversion = ADC_OPERATING_CHANNELS,
		.DMAContinuousRequests = ENABLE,
		.EOCSelection = ADC_EOC_SEQ_CONV,
	};

	memcpy(adcc->active_channels, active_channels, sizeof(active_channels));
	memcpy(adcc->channels, channels, sizeof(channels));
	memcpy(&adcc->ADC_InitTemplate, &init, sizeof(init));


	ADC_UpdateSamplingFrequency(adcc, _500_Hz);

	HAL_StatusTypeDef status;
	memcpy(&adcc->hadc->Init, &adcc->ADC_InitTemplate, sizeof(adcc->ADC_InitTemplate));
	status = HAL_ADC_Init(adcc->hadc);
	if(status != HAL_OK) return status;
	uint8_t i;
	for(i=0; i<ADC_OPERATING_CHANNELS; i++) {
		status = HAL_ADC_ConfigChannel(adcc->hadc, &adcc->channels[i].config);
		if (status != HAL_OK) return status;
	}
	return HAL_OK;
}

uint8_t ADC_GetFrequency(ADC_Controller *adcc) {
	return adcc->frequency;
}

HAL_StatusTypeDef ADC_UpdateSamplingFrequency(ADC_Controller *adcc, uint8_t frequency) {
	adcc->frequency=frequency;
	__HAL_TIM_SET_AUTORELOAD(adcc->htim, ADC_TimerPeriods[frequency]);
	return HAL_OK;
}

uint8_t ADC_GetCntOfActiveChannels(ADC_Controller *adcc) {
	return adcc->active_channels_count;
}

HAL_StatusTypeDef ADC_UpdateChannels(ADC_Controller *adcc, uint8_t *channels_active) {
	HAL_StatusTypeDef status;
	if(!adcc->adc_running) {
		adcc->hadc->Init.NbrOfConversion=adcc->active_channels_count=0;
		uint8_t i;
		for(i=0; i<ADC_OPERATING_CHANNELS; i++) {
			adcc->channels[i].active=channels_active[i];
			if(adcc->channels[i].active) adcc->hadc->Init.NbrOfConversion=++adcc->active_channels_count;
		}
		HAL_ADC_Init(adcc->hadc);
		for (i = 0; i < ADC_OPERATING_CHANNELS; i++)
			if (adcc->channels[i].active) {
				status = HAL_ADC_ConfigChannel(adcc->hadc, &adcc->channels[i].config);
				if(status != HAL_OK) return status;
			}
		adcc->total_data_length = (ADC_SAMPLES_PER_PACKAGE / adcc->active_channels_count);
		adcc->total_data_length *= adcc->active_channels_count;
		return HAL_OK;
	}
	return HAL_ERROR;
}

HAL_StatusTypeDef ADC_DisableChannel(ADC_Controller *adcc, ADC_AdvChannelConfTypeDef *channel) {
	HAL_StatusTypeDef status;
	channel->active = 0;
	adcc->hadc->Init.NbrOfConversion = --adcc->active_channels_count;
	uint8_t i;
	HAL_ADC_Init(adcc->hadc);
	for (i = 0; i < ADC_OPERATING_CHANNELS; i++)
		if (adcc->channels[i].active) {
			status = HAL_ADC_ConfigChannel(adcc->hadc, &adcc->channels[i].config);
			if(status != HAL_OK) return status;
		}
	return HAL_OK;
}

HAL_StatusTypeDef ADC_EnableChannel(ADC_Controller *adcc, ADC_AdvChannelConfTypeDef *channel) {
	HAL_StatusTypeDef status = HAL_OK;
	channel->active = 1;
	adcc->hadc->Init.NbrOfConversion = ++adcc->active_channels_count;
	uint8_t i;
	status = HAL_ADC_Init(adcc->hadc);
	for (i = 0; i < ADC_OPERATING_CHANNELS; i++)
		if (adcc->channels[i].active)
			status = status == HAL_OK ? HAL_ADC_ConfigChannel(adcc->hadc, &adcc->channels[i].config) : status;
	return status;
}

HAL_StatusTypeDef ADC_StartConversion(ADC_Controller *adcc) {
	HAL_StatusTypeDef status = HAL_OK;
	ADC_DATA_DATA_BYTE_BUFFER[START_BYTE_OFFSET]=START;
	ADC_DATA_DATA_BYTE_BUFFER[SERVICE_BYTE_OFFSET]=ADC_SRV;
	ADC_DATA_DATA_BYTE_BUFFER[CHARACTERISTIC_BYTE_OFFSET]=ADC_DATA;
	ADC_DATA_DATA_BYTE_BUFFER[LENGTH_BYTE_OFFSET]=_256_B;
	ADC_DATA_DATA_BYTE_BUFFER[261]=STOP;

	if(!adcc->adc_running) {
		adcc->sample_cnt = 0;
//		SDCard_CreateNewMeasurementFile(SDCard_GetSDCardController());
		status = HAL_ADC_Start_DMA(adcc->hadc, (uint32_t*)adcc->sample_buffer, adcc->total_data_length);
		adcc->timer_int=0;
		status = status == HAL_OK ? HAL_TIM_Base_Start_IT(adcc->htim) : status;
		adcc->adc_running=1;
	}
	return status;
}

HAL_StatusTypeDef ADC_EndConversion(ADC_Controller *adcc) {
	HAL_StatusTypeDef status = HAL_OK;
	if(adcc->adc_running) {
		status = HAL_TIM_Base_Stop_IT(adcc->htim);
		status = status == HAL_OK ? HAL_ADC_Stop_DMA(adcc->hadc) : status;
		adcc->adc_running=0;
	}
	return status;
}

HAL_StatusTypeDef ADC_TogglePauseConversion(ADC_Controller *adcc, uint8_t on_off) {
	HAL_StatusTypeDef status = HAL_OK;
	if(on_off) {
		status = HAL_TIM_Base_Stop_IT(adcc->htim);
		adcc->adc_pause=1;
	} else {
		status = HAL_TIM_Base_Start_IT(adcc->htim);
		adcc->adc_pause=0;
	}
	return status;
}


ADC_Controller* ADC_GetADCController() {
	return &adcc;
}

ADC_HandleTypeDef* ADC_GetADCHandle() {
	return ADC_GetADCController()->hadc;
}

TIM_HandleTypeDef* ADC_GetADCTimerHandle(ADC_Controller *adcc) {
	return adcc->htim;
}

HAL_StatusTypeDef HAL_ADC_DeconfigChannel(ADC_HandleTypeDef *hadc,
		ADC_ChannelConfTypeDef *sConfig) {

	/* Check the parameters */
	assert_param(IS_ADC_CHANNEL(sConfig->Channel));
	assert_param(IS_ADC_REGULAR_RANK(sConfig->Rank));
	assert_param(IS_ADC_SAMPLE_TIME(sConfig->SamplingTime));

	/* Process locked */
	__HAL_LOCK(hadc);

	/* if ADC_Channel_10 ... ADC_Channel_18 is selected */
	if (sConfig->Channel > ADC_CHANNEL_9) {
		/* Clear the old sample time */
		hadc->Instance->SMPR1 &= ~ADC_SMPR1(ADC_SMPR1_SMP10, sConfig->Channel);
	} else /* ADC_Channel include in ADC_Channel_[0..9] */{
		/* Clear the old sample time */
		hadc->Instance->SMPR2 &= ~ADC_SMPR2(ADC_SMPR2_SMP0, sConfig->Channel);
	}

	/* For Rank 1 to 6 */
	if (sConfig->Rank < 7U) {
		/* Clear the old SQx bits for the selected rank */
		hadc->Instance->SQR3 &= ~ADC_SQR3_RK(ADC_SQR3_SQ1, sConfig->Rank);
	}
	/* For Rank 7 to 12 */
	else if (sConfig->Rank < 13U) {
		/* Clear the old SQx bits for the selected rank */
		hadc->Instance->SQR2 &= ~ADC_SQR2_RK(ADC_SQR2_SQ7, sConfig->Rank);
	}
	/* For Rank 13 to 16 */
	else {
		/* Clear the old SQx bits for the selected rank */
		hadc->Instance->SQR1 &= ~ADC_SQR1_RK(ADC_SQR1_SQ13, sConfig->Rank);
	}

	/* Process unlocked */
	__HAL_UNLOCK(hadc);

	/* Return function status */
	return HAL_OK;
}

void ADC_CharModifiedCallback(ADC_CharType char_type, uint8_t *Attr_Data, uint16_t Attr_Data_Length) {
//	ADC_Controller* adcc = ADC_GetADCController();
//	switch(char_type) {
//	case ADC_CHAR_FREQUENCY: {
//		if(!adcc->adc_running || adcc->adc_pause)
//			if(Attr_Data != NULL && Attr_Data_Length == 1 && Attr_Data[0] < ADC_FREQUENCIES_VARIANTS)
//				ADC_UpdateSamplingFrequency(adcc, Attr_Data[0]);
//		break;
//	}
//	case ADC_CHAR_ACTIVE_CHANNELS: {
//		uint8_t i;
//		if(!adcc->adc_running)
//			if(Attr_Data != NULL && Attr_Data_Length == 8) {
//				for(i=0; i<Attr_Data_Length; i++)
//					adcc->channels[i].active = Attr_Data[i];
//				ADC_UpdateChannels(adcc);
//			}
//		break;
//	}
//	case ADC_CHAR_RUNNING: {
//		if(Attr_Data != NULL && Attr_Data_Length == 1) {
//			if(adcc->adc_running != Attr_Data[0])
//				adcc->adc_running = Attr_Data[0];
//			else return;
//		}
//		if(adcc->adc_running) ADC_StartConversion(adcc);
//		else ADC_EndConversion(adcc);
//		break;
//	}
//	case ADC_CHAR_PAUSE: {
//		if(adcc->adc_running)
//			if(Attr_Data != NULL && Attr_Data_Length == 1)
//				if(adcc->adc_pause != Attr_Data[0])
//					adcc->adc_pause=Attr_Data[0];
//		ADC_TogglePauseConversion(adcc);
//		break;
//	}
//	case ADC_CHAR_SD_CARD_FLOAT_RESOLUTION: {
//		if(!adcc->adc_running)
//			if(Attr_Data != NULL && Attr_Data_Length == 1)
//				if(adcc->float_resolution != Attr_Data[0])
//					if(ADC_FLOAT_MIN_RESOLUTION <= Attr_Data[0] <= ADC_FLOAT_MAX_RESOLUTION)
//						adcc->float_resolution = Attr_Data[0];
//		break;
//	}
//	default: return;
//	}
}

void _12bitTo2Bytes6bitParser(uint8_t *to, uint16_t to_sizeInBytes, uint16_t *src, uint16_t src_sizeInWords) {
	uint16_t i;
	for(i=0; i<src_sizeInWords; i++) {
		to[i*2]=(src[i]&0x0FC0)>>6;
		to[i*2+1]=src[i]&0x3F;
	}
}

void ADCInterruptCallback() {
	ADC_Controller *adcc = ADC_GetADCController();
	ADC_TogglePauseConversion(adcc, 1);
		adcc->frames_produced++;
//		SDCard_WriteSamplesToFile(SDCard_GetSDCardController());
		_12bitTo2Bytes6bitParser(ADC_DATA_DATA_BYTE_BUFFER+RESPONSE_DATA_BYTE_OFFSET, 256, adcc->sample_buffer, ADC_SAMPLES_PER_PACKAGE);
		BL_RFCOMM_Response(BL_RFCOMM_GetBLRfComm(), ADC_DATA_DATA_BYTE_BUFFER, sizeof(ADC_DATA_DATA_BYTE_BUFFER),0);
		adcc->sample_cnt=0;
	ADC_TogglePauseConversion(adcc, 0);
}
