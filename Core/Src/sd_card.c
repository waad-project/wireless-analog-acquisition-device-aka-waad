///*
// * sd_card.c
// *
// *  Created on: Oct 8, 2020
// *      Author: kaczp
// */
//
//#include "sd_card.h"
//#include "adc.h"
//#include "fatfs.h"
//#include "ff.h"
//#include "stdlib.h"
//#include "string.h"
//
//extern RTC_HandleTypeDef hrtc;
//
//uint8_t SD_CARD_BUFFER_WRITE[400];
//
//uint8_t SDCARD_FILE_NAME_TEMPLATE[60] =
//		"H%02dM%02dS%02d_D%02dM%02dY20%02d.txt";
//
//
//uint8_t SDCARD_FILE_HEADER_TEMPLATE[300] =
//		"///////////////////////////////////////////\n"
//		"// %02d:%02d:%02d %02d/%02d/20%02d, %.3s //\n"
//		"// Frequency: %4d Hz                     //\n"
//		"// Resolution: 12b                       //\n"
//		"///////////////////////////////////////////\n";
//
//uint8_t SDCARD_SAMPLES_DYNAMIC_TEMPLATE[50];
//
//
//uint8_t _1_channel_buffer[10] =
//		"%d\n";
//uint8_t _2_channel_buffer[15] =
//		"%d;%d\n";
//uint8_t _3_channel_buffer[20] =
//		"%d;%d;%d\n";
//uint8_t _4_channel_buffer[25] =
//		"%d;%d;%d;%d\n";
//uint8_t _5_channel_buffer[30] =
//		"%d;%d;%d;%d;%d\n";
//uint8_t _6_channel_buffer[35] =
//		"%d;%d;%d;%d;%d;%d\n";
//uint8_t _7_channel_buffer[40] =
//		"%d;%d;%d;%d;%d;%d;%d\n";
//uint8_t _8_channel_buffer[45] =
//		"%d;%d;%d;%d;%d;%d;%d;%d\n";
//
//uint8_t *SD_CARD_CHANNEL_BUFFERS[ADC_OPERATING_CHANNELS] = {
//		_1_channel_buffer,
//		_2_channel_buffer,
//		_3_channel_buffer,
//		_4_channel_buffer,
//		_5_channel_buffer,
//		_6_channel_buffer,
//		_7_channel_buffer,
//		_8_channel_buffer
//};
//
//uint8_t weekdays[7][4] = {
//	"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
//};
//
//SD_CARD_Controller sdcc;
//
//SD_CARD_Controller* SDCard_GetSDCardController() {
//	return &sdcc;
//	f_mount(&SDFatFS, SDPath, 0);
//}
//
//uint8_t SDCard_IsOverflowed(SD_CARD_Controller *sdcc) {
//
//	return 0;
//}
//
//HAL_StatusTypeDef SDCard_Format(SD_CARD_Controller *sdcc) {
//	FRESULT result;
//	result = f_mount(NULL, "", 0);
//	//TODO: fix this shit
//	//result = f_mkfs("", 0, FM_FAT32, 0, 	);
//
//	if(result != FR_OK) return result;
//
//	result = f_mount(&SDFatFS, SDPath, 0);
//	if(result != FR_OK) return result;
//
//	if(result != FR_OK) return HAL_ERROR;
//	else return HAL_OK;
//}
//
//void SDCard_PepareBufferForFileHeader(SD_CARD_Controller *sdcc) {
//	memset(SD_CARD_BUFFER_WRITE,'\0' ,sizeof(SD_CARD_BUFFER_WRITE));
//	HAL_RTC_GetTime(&hrtc, &sdcc->sTime, RTC_FORMAT_BCD);
//	HAL_RTC_GetDate(&hrtc, &sdcc->sDate, RTC_FORMAT_BCD);
//	sprintf(SD_CARD_BUFFER_WRITE, SDCARD_FILE_HEADER_TEMPLATE,
//			sdcc->sTime.Hours,
//			sdcc->sTime.Minutes,
//			sdcc->sTime.Seconds,
//			sdcc->sDate.Date,
//			sdcc->sDate.Month,
//			sdcc->sDate.Year,
//			weekdays[sdcc->sDate.WeekDay],
//			ADC_Frequencies[ADC_GetADCController()->frequency]
//	);
//}
//void SDCard_PepareBufferForSamplePackage(SD_CARD_Controller *sdcc) {
//	uint8_t  index = ADC_GetADCController()->active_channels_count-1;
//	memset(SDCARD_SAMPLES_DYNAMIC_TEMPLATE,'\0' ,sizeof(SDCARD_SAMPLES_DYNAMIC_TEMPLATE));
//	memcpy(SDCARD_SAMPLES_DYNAMIC_TEMPLATE, SD_CARD_CHANNEL_BUFFERS[index], strlen(SD_CARD_CHANNEL_BUFFERS[index]));
//}
//void SDCard_FillBufferWithSamplePackage() {
//	ADC_Controller* adcc = ADC_GetADCController();
//	uint8_t i, max_it = (adcc->total_data_length/adcc->active_channels_count), index = adcc->active_channels_count-1;
//	memset(SD_CARD_BUFFER_WRITE,'\0' ,sizeof(SD_CARD_BUFFER_WRITE));
//	switch(adcc->active_channels_count) {
//	case 1: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_1_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 2: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_2_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 3: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_3_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 4: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_4_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 5: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_5_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 6: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_6_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 7: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_7_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	case 8: {
//				for(i = 0; i < max_it; i++)
//					sprintf(SD_CARD_BUFFER_WRITE+i*adcc->active_channels_count,SDCARD_SAMPLES_DYNAMIC_TEMPLATE,UNLOAD_8_ELEMENTS(adcc->sample_buffer+i*adcc->active_channels_count));
//				break;
//			}
//	}
//}
//
//
//HAL_StatusTypeDef SDCard_SetTimestamp(SD_CARD_Controller *sdcc, uint8_t *data) {
//	HAL_StatusTypeDef state;
//	sdcc->sTime = (RTC_TimeTypeDef){data[0], data[1], data[2]};
//	sdcc->sDate = (RTC_DateTypeDef){data[6], data[4], data[3], data[5]};
//	state = HAL_RTC_SetTime(&hrtc, &sdcc->sTime, RTC_FORMAT_BCD);
//	if(state != HAL_OK) return state;
//	state = HAL_RTC_SetDate(&hrtc, &sdcc->sDate, RTC_FORMAT_BCD);
//	if(state != HAL_OK) return state;
//	return HAL_OK;
//}
//
//HAL_StatusTypeDef SDCard_CreateNewMeasurementFile(SD_CARD_Controller *sdcc) {
//	FRESULT result;
//	SDCard_PepareBufferForFileHeader(sdcc);
//	memset(SD_CARD_BUFFER_WRITE,'\0' ,sizeof(SD_CARD_BUFFER_WRITE));
//	sprintf(SD_CARD_BUFFER_WRITE,SDCARD_FILE_NAME_TEMPLATE,sdcc->sTime.Hours,
//			sdcc->sTime.Minutes,
//			sdcc->sTime.Seconds,
//			sdcc->sDate.Date,
//			sdcc->sDate.Month,
//			sdcc->sDate.Year);
//	result = f_open(&SDFile, SD_CARD_BUFFER_WRITE, FA_WRITE | FA_CREATE_NEW | FA_OPEN_ALWAYS);
//	if(result != FR_OK) return HAL_ERROR;
//	SDCard_PepareBufferForSamplePackage(sdcc);
//	result = f_write(&SDFile, SD_CARD_BUFFER_WRITE, (UINT)sizeof(SD_CARD_BUFFER_WRITE), &sdcc->bw);
//	if(result != FR_OK) return HAL_ERROR;
//	sdcc->file_opened_f = 1;
//	return HAL_OK;
//}
//HAL_StatusTypeDef SDCard_CloseMeasurementFile(SD_CARD_Controller *sdcc) {
//	FRESULT result;
//	if(sdcc->file_opened_f) {
//		result = f_close(&SDFile);
//		if(result != FR_OK) return HAL_ERROR;
//	}
//	return HAL_OK;
//}
//
//void SDCard_WriteSamplesToFile(SD_CARD_Controller *sdcc) {
//	SDCard_FillBufferWithSamplePackage();
//	if(sdcc->file_opened_f)
//		f_write(&SDFile, SD_CARD_BUFFER_WRITE, (UINT)strlen(SD_CARD_BUFFER_WRITE), &sdcc->bw);
//}
