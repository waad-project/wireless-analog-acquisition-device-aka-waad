/*
 * bl_rfcomm.c
 *
 *  Created on: Oct 21, 2020
 *      Author: kaczp
 */

#include "bl_rfcomm.h"
#include "main.h"
#include "stm32f4xx_hal.h"
#include "semphr.h"
#include "bl_api_controller.h"

BL_RfComm blrf;

uint8_t CONFIRM_CONNECT_FRAME[6] = {START, 0, 0, 0, 0, STOP};
uint8_t HANDSHAKE_CONNECT_FRAME[6] = {START, 1, 1, 1, 1, STOP};

extern UART_HandleTypeDef huart2;

void BL_RFCOMM_Init(BL_RfComm* blrf, UART_HandleTypeDef* huart, osThreadId_t blrfCommTaskHandle, osThreadId_t executionTask) {
	HAL_NVIC_DisableIRQ(EXTI2_IRQn);
	blrf->huart=huart;
	blrf->pending_con_f=1;
	blrf->pending_rq_f=0;
	blrf->unsuccessful_connections=0;
	blrf->blrfCommTaskHandle=blrfCommTaskHandle;
	blrf->executionTask=executionTask;
	BL_RFCOMM_WaitUntilUART_Ready(blrf, 10);
	HAL_NVIC_EnableIRQ(EXTI2_IRQn);
}

uint8_t BL_RFCOMM_CheckIfLostConnection(BL_RfComm* blrf) {
	if(blrf->pos_lost_of_con_f) {
		if(HAL_GPIO_ReadPin(HC_06_STATE_GPIO_Port, HC_06_STATE_Pin) == GPIO_PIN_RESET) {
			osDelay(10);
			if(HAL_GPIO_ReadPin(HC_06_STATE_GPIO_Port, HC_06_STATE_Pin) == GPIO_PIN_RESET) {
				blrf->con_f=0;
				return 0;
			}
		}
	}
	return 1;
}

void BL_RFCOMM_GetRequest(BL_RfComm* blrf) {
	if(!blrf->pending_long_data_rq_f) {
		if(blrf->long_data_f) {
			HAL_UART_AbortReceive_IT(blrf->huart);
			taskENTER_CRITICAL();
			blrf->pending_rq_f = 0;
			blrf->long_data_f = 0;
			blrf->pending_long_data_rq_f=1;
			BL_RFCOMM_WaitUntilUART_RX_Ready(blrf, 1);
			HAL_UART_Receive_DMA(blrf->huart, blrf->long_data_receive_buffer, blrf->long_data_length+5);
			taskEXIT_CRITICAL();
			return;
		}
	} else return;
	if(!blrf->pending_rq_f) {
		HAL_UART_AbortReceive_IT(blrf->huart);
		taskENTER_CRITICAL();
		blrf->pending_rq_f = 1;
		BL_RFCOMM_WaitUntilUART_RX_Ready(blrf, 1);
		HAL_UART_Receive_DMA(blrf->huart, blrf->receive_buffer, sizeof(blrf->receive_buffer));
		taskEXIT_CRITICAL();
	}
}

void BL_RFCOMM_Response(BL_RfComm* blrf, uint8_t* data, uint16_t length, uint8_t clearPendingFlag) {
	BL_RFCOMM_WaitUntilUART_TX_Ready(blrf, 1);
	HAL_UART_Transmit_DMA(blrf->huart, data, length);
}

void BL_RFCOMM_WaitUntilUART_Ready(BL_RfComm* blrf,uint32_t Interval) {
	while(HAL_UART_GetState(blrf->huart)!=HAL_UART_STATE_READY) {
		osDelay(Interval);
	}
}

void BL_RFCOMM_WaitUntilUART_TX_Ready(BL_RfComm* blrf,uint32_t Interval) {
	HAL_UART_StateTypeDef state = HAL_UART_GetState(blrf->huart);
	while(state==HAL_UART_STATE_BUSY_TX || state==HAL_UART_STATE_BUSY_TX_RX) {
		osDelay(Interval);
	}
}

void BL_RFCOMM_WaitUntilUART_RX_Ready(BL_RfComm* blrf,uint32_t Interval) {
	HAL_UART_StateTypeDef state = HAL_UART_GetState(blrf->huart);
	while(state==HAL_UART_STATE_BUSY_RX || state==HAL_UART_STATE_BUSY_TX_RX) {
		osDelay(Interval);
	}
}

uint8_t BL_RFCOMM_Connected(BL_RfComm* blrf) {
	return blrf->con_f;
}

void BL_RFCOMM_ResetFlags(BL_RfComm* blrf) {
	blrf->pending_con_f=1;
	blrf->con_f=0;
	blrf->pos_con_f=0;
	blrf->pos_lost_of_con_f=0;
	blrf->unsuccessful_connections++;
}

uint8_t BL_RFCOMM_WaitUntilConnectedState(BL_RfComm* blrf, uint32_t Interval, uint32_t total_timeout) {
	if(HAL_GPIO_ReadPin(HC_06_STATE_GPIO_Port, HC_06_STATE_Pin)==GPIO_PIN_SET) {
		blrf->pos_con_f=1;
		return 1;
	} else {
		xTaskNotifyWait(0, 0, NULL, portMAX_DELAY);
		uint32_t timeout = 0;
		while(timeout<total_timeout) {
			if(HAL_GPIO_ReadPin(HC_06_STATE_GPIO_Port, HC_06_STATE_Pin)==GPIO_PIN_SET) {
				blrf->pos_con_f=1;
				return 1;
			}
			osDelay(Interval);
			timeout+=Interval;
		}
	}
	return 0;
}

void BL_RFCOMM_TryToConnect(BL_RfComm* blrf, uint32_t Interval, uint32_t total_timeout) {
	BL_RFCOMM_WaitUntilUART_RX_Ready(blrf,1);
	HAL_UART_Receive_DMA(blrf->huart, blrf->receive_buffer, sizeof(blrf->receive_buffer));
	while(!blrf->con_f) BL_RFCOMM_WaitUntilConnectionConfirmed(blrf, Interval, total_timeout);
}

void BL_RFCOMM_WaitUntilConnectionConfirmed(BL_RfComm* blrf,uint32_t Interval, uint32_t total_timeout) {
	uint32_t timeout=0;
	while(timeout<total_timeout && !blrf->con_f) {
		BL_RFCOMM_WaitUntilUART_TX_Ready(blrf,1);
		HAL_UART_Transmit_DMA(blrf->huart, CONFIRM_CONNECT_FRAME, sizeof(CONFIRM_CONNECT_FRAME));
		timeout+=Interval;
		blrf->pos_con_f=1;
		osDelay(Interval);
		uint8_t i;
		for(i=0; i<sizeof(blrf->receive_buffer); i++)
			if(blrf->receive_buffer[i]!=CONFIRM_CONNECT_FRAME[i]) {
				blrf->pos_con_f=0;
				blrf->unsuccessful_connections++;
				i=sizeof(blrf->receive_buffer);
			}
		if(blrf->pos_con_f) {
			BL_RFCOMM_WaitUntilUART_TX_Ready(blrf,1);
			HAL_UART_Transmit_DMA(blrf->huart, HANDSHAKE_CONNECT_FRAME, sizeof(HANDSHAKE_CONNECT_FRAME));
			blrf->con_f=1;
		}
	}
}

BL_RfComm* BL_RFCOMM_GetBLRfComm() {
	return &blrf;
}

UART_HandleTypeDef* BL_RFCOMM_GetUARTHandle() {
	return BL_RFCOMM_GetBLRfComm()->huart;
}

void BL_RFCOMM_ConnectionChangedStateCallback() {
	BL_RfComm* blrf = BL_RFCOMM_GetBLRfComm();
	if(blrf->pending_con_f) {
		blrf->pending_con_f=0;
		xTaskGenericNotifyFromISR(blrf->blrfCommTaskHandle, 0, eNoAction, NULL, pdFALSE);
	}
	else if(blrf->con_f) blrf->pos_lost_of_con_f=1;
}

void BL_RFCOMM_RequestReceivedCallback() {
	BL_RfComm* blrf = BL_RFCOMM_GetBLRfComm();
	if(blrf->pending_rq_f || blrf->pending_long_data_rq_f) osSemaphoreRelease(BL_API_GetBL_API_Controller()->executionBinarySemaphoreHandle);
}

void BL_RFCOMM_PossibleConnectionConfirmCallback() {
	BL_RfComm* blrf = BL_RFCOMM_GetBLRfComm();
	blrf->pos_con_f=1;
}



