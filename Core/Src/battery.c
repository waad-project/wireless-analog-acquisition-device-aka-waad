/*
 * battery.c
 *
 *  Created on: Oct 10, 2020
 *      Author: kaczp
 */

#include "battery.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"
#include "bl_api_controller.h"


BATTERY_Controller batc;

BATTERY_Controller *BATTERY_GetBatteryController() {
	return &batc;
}

TIM_HandleTypeDef *BATTERY_GetBatteryTimerHandle(BATTERY_Controller *batc) {
	return batc->htim;
}

void BATTERY_ControllerInit(BATTERY_Controller *batc, ADC_HandleTypeDef *hadc, TIM_HandleTypeDef *htim, osThreadId_t batteryTaskHandle, osSemaphoreId_t batteryBinarySemaphoreHandle) {
	batc->batteryTaskHandle = batteryTaskHandle;
	batc->batteryBinarySemaphoreHandle = batteryBinarySemaphoreHandle;
	batc->hadc = hadc;
	batc->htim = htim;
}

void BATTERY_StartConversion(BATTERY_Controller *batc) {
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[START_BYTE_OFFSET]=START;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[SERVICE_BYTE_OFFSET]=START;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[CHARACTERISTIC_BYTE_OFFSET]=START;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[LENGTH_BYTE_OFFSET]=START;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[sizeof(BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER)-1]=STOP;

	BATTERY_SINGLE_DATA_BYTE_BUFFER[START_BYTE_OFFSET]=START;
	BATTERY_SINGLE_DATA_BYTE_BUFFER[SERVICE_BYTE_OFFSET]=START;
	BATTERY_SINGLE_DATA_BYTE_BUFFER[CHARACTERISTIC_BYTE_OFFSET]=START;
	BATTERY_SINGLE_DATA_BYTE_BUFFER[LENGTH_BYTE_OFFSET]=START;
	BATTERY_SINGLE_DATA_BYTE_BUFFER[sizeof(BATTERY_SINGLE_DATA_BYTE_BUFFER)-1]=STOP;
	if(!batc->adc_sampling) {
		HAL_ADC_Start_DMA(batc->hadc, (uint32_t*)batc->samples_buf, BATTERY_BUFFER_SIZE);
		HAL_TIM_Base_Start_IT(batc->htim);
		batc->adc_sampling=1;
	}
}

void BATTERY_StopConversion(BATTERY_Controller *batc) {
	if(batc->adc_sampling) {
		HAL_TIM_Base_Stop_IT(batc->htim);
		HAL_ADC_Stop_DMA(batc->hadc);
		batc->adc_sampling=0;
	}
}

float BATTERY_GetReadingsAverage(BATTERY_Controller *batc) {
	uint8_t i;
	uint32_t sum=0;
	for(i=0; i<batc->samples_cnt; i++) {
		if(batc->samples_buf[i] == 0) return (float)(sum/(float)i);
		sum+=batc->samples_buf[i];
	}
	return (float)(sum/(float)batc->samples_cnt);
}

void BATTERY_GetPercent(BATTERY_Controller *batc) {
	HAL_TIM_Base_Stop_IT(batc->htim);
	float offset = BATTERY_REF_MIN;
	float range = BATTERY_RANGE;
	float average = BATTERY_GetReadingsAverage(batc);
	float percent = (average-offset)/range*100;
	batc->percent = (uint8_t)percent;
	batc->percent = batc->percent < 0 ? 0 : batc->percent;
	batc->percent = batc->percent > 100 ? 100 : batc->percent;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[RESPONSE_DATA_BYTE_OFFSET] = (batc->percent&0xF0)>>4;
	BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[RESPONSE_DATA_BYTE_OFFSET+1] = batc->percent&0x0F;
	//if(BL_RFCOMM_GetBLRfComm()->con_f) BL_RFCOMM_Response(BL_RFCOMM_GetBLRfComm(), BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER, sizeof(BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER), 0);
	HAL_TIM_Base_Start_IT(batc->htim);
}

uint8_t BATTERY_GetPowerLevel(BATTERY_Controller *batc) {
	return batc->percent;
}

uint8_t BATTERY_GetChargeState(BATTERY_Controller *batc) {
	return batc->is_charged;
}

void BATTERY_CheckIfPluggedIn(BATTERY_Controller *batc) {
	if(batc->possible_charge_state_change_f) {
		batc->is_charged = HAL_GPIO_ReadPin(CHARGER_INPUT_GPIO_Port, CHARGER_INPUT_Pin);
		osDelay(50);
		batc->is_charged = HAL_GPIO_ReadPin(CHARGER_INPUT_GPIO_Port, CHARGER_INPUT_Pin);
		BATTERY_SINGLE_DATA_BYTE_BUFFER[RESPONSE_DATA_BYTE_OFFSET] = batc->is_charged;
		if(BL_RFCOMM_GetBLRfComm()->con_f) BL_RFCOMM_Response(BL_RFCOMM_GetBLRfComm(), BATTERY_SINGLE_DATA_BYTE_BUFFER, sizeof(BATTERY_SINGLE_DATA_BYTE_BUFFER), 0);
		batc->possible_charge_state_change_f = 0;
	}
}


void BATTERY_ConversionCompleteCallback() {
	BATTERY_Controller *batc = BATTERY_GetBatteryController();
	if(batc->samples_index >= BATTERY_BUFFER_SIZE) {
		osSemaphoreRelease(batc->batteryBinarySemaphoreHandle);
		batc->samples_index=0;
	}
	else batc->samples_index++;

	if(batc->samples_cnt < BATTERY_BUFFER_SIZE) batc->samples_cnt++;
}

void BATTERY_ChargeStateChangeCallback() {
	BATTERY_Controller *batc = BATTERY_GetBatteryController();
	batc->possible_charge_state_change_f=1;
}
