/*
 * bl_api_controller.c
 *
 *  Created on: Oct 22, 2020
 *      Author: kaczp
 */

#include "bl_api_controller.h"
#include "bl_rfcomm.h"
#include "adc.h"
#include "battery.h"
#include "sd_card.h"
#include "string.h"

BL_API_Controller blap;

uint8_t BATTERY_SINGLE_DATA_BYTE_BUFFER[7];
uint8_t BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER[8];

uint8_t SD_CARD_SINGLE_DATA_BYTE_BUFFER[7];
uint8_t SD_CARD_TIMESTAMP_DATA_BYTE_BUFFER[14];

uint8_t ADC_SINGLE_DATA_BYTE_BUFFER[7];
uint8_t ADC_CHANNELS_ACTIVE_DATA_BYTE_BUFFER[14];
uint8_t ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER[14];
uint8_t ADC_DATA_DATA_BYTE_BUFFER[262];

uint8_t CUSTOM_DATA_SINGLE_DATA_BYTE_BUFFER[7];

const uint16_t DATA_LENGTH_BYTES_VALUES[9] = {
	1, 2, 4, 8, 16, 32, 64, 128, 256
};


void BL_API_InitController(BL_API_Controller* blap, BL_RfComm* blrf, osSemaphoreId_t executionBinarySemaphoreHandle) {
	blap->blrf=blrf;
	blap->executionBinarySemaphoreHandle = executionBinarySemaphoreHandle;
}

BL_API_Controller* BL_API_GetBL_API_Controller() {
	return &blap;
}

uint8_t BL_API_ValidateFrame(BL_API_Controller* blap) {

	if(blap->blrf->pending_long_data_rq_f) {
		return blap->blrf->long_data_receive_buffer[START_BYTE_OFFSET]==START && blap->blrf->long_data_receive_buffer[blap->blrf->long_data_length+4]==STOP;
	} else {
		return blap->blrf->receive_buffer[START_BYTE_OFFSET]==START && blap->blrf->receive_buffer[STOP_BYTE_OFFSET]==STOP;
	}

}

void BL_API_DecodeRequest(BL_API_Controller* blap) {
	BL_RfComm* blrf = BL_RFCOMM_GetBLRfComm();
	if(BL_API_ValidateFrame(blap)) {
		if(blap->blrf->pending_long_data_rq_f) {
			switch(blap->blrf->long_data_receive_buffer[SERVICE_BYTE_OFFSET])
				case ADC_SRV: {
				ADC_Controller* adcc = ADC_GetADCController();
				switch(blap->blrf->long_data_receive_buffer[CHARACTERISTIC_BYTE_OFFSET]) {
					case ADC_CHANNELS_ACTIVE: { //DONE
						if(blap->blrf->long_data_receive_buffer[RW_BYTE_OFFSET]) { //WRITE
							uint8_t tmp[7] = {START, ADC_SRV, ADC_CHANNELS_ACTIVE, WRITE, _1_B, 0, STOP};
							tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_UpdateChannels(adcc,blap->blrf->long_data_receive_buffer+REQUEST_DATA_BYTE_OFFSET)==HAL_OK ? 1 : 0;
							taskENTER_CRITICAL();
							blrf->pending_rq_f = 0;
							blrf->pending_long_data_rq_f = 0;
							taskEXIT_CRITICAL();
							blrf->long_data_length = 0;
							blrf->long_data_f = 0;
							memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
							BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),1);
							return;
						}
						break;
					}
				default: break;
				}
			}
		}
		switch(blap->blrf->receive_buffer[SERVICE_BYTE_OFFSET]) {
		case BATT_SRV: { //DONE
			BATTERY_Controller* batc = BATTERY_GetBatteryController();
			switch(blap->blrf->receive_buffer[CHARACTERISTIC_BYTE_OFFSET]) {
			case BATTERY_POWER_LEVEL: { //DONE
				if(!blap->blrf->receive_buffer[RW_BYTE_OFFSET]) { //READ
					uint8_t power_level=BATTERY_GetPowerLevel(batc);
					uint8_t tmp[8] = {START, BATT_SRV, BATTERY_POWER_LEVEL, READ, _2_B, power_level>>4&0x0F, power_level&0x0F, STOP};
					memcpy(BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER, tmp, sizeof(BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER, sizeof(BATTERY_POWER_LEVEL_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			case BATTERY_CHARGED: { //DONE
				if(!blap->blrf->receive_buffer[RW_BYTE_OFFSET]) { //READ
					uint8_t charge_state=BATTERY_GetChargeState(batc);
					uint8_t tmp[7] = {START, BATT_SRV, BATTERY_CHARGED, READ, _1_B, charge_state, STOP};
					memcpy(BATTERY_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(BATTERY_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, BATTERY_SINGLE_DATA_BYTE_BUFFER, sizeof(BATTERY_SINGLE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			default: return;
			}
			break;
		}
		case SD_SRV: {
//			SD_CARD_Controller* sdcc = SDCard_GetSDCardController();
//			switch(blap->blrf->receive_buffer[CHARACTERISTIC_BYTE_OFFSET]) {
//			case SDCARD_TIMESTAMP: {
//				if(blap->blrf->long_data_receive_buffer[RW_BYTE_OFFSET]) { //WRITE
//					uint8_t tmp[7] = {START, SD_SRV, SDCARD_TIMESTAMP, WRITE, _1_B, 0, STOP};
//					//hour, minute, second, day, month, year, week_day
//					tmp[RESPONSE_DATA_BYTE_OFFSET] = SDCard_SetTimestamp(sdcc, blap->blrf->long_data_receive_buffer+REQUEST_DATA_BYTE_OFFSET) == HAL_OK ? 1 : 0;
//					memcpy(SD_CARD_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(SD_CARD_SINGLE_DATA_BYTE_BUFFER));
//					BL_RFCOMM_Response(blrf, SD_CARD_SINGLE_DATA_BYTE_BUFFER, sizeof(SD_CARD_SINGLE_DATA_BYTE_BUFFER),1);
//				}
//				else {
//					uint8_t tmp[14] = {START, SD_SRV, SDCARD_TIMESTAMP, READ, _8_B, sdcc->sTime.Hours, sdcc->sTime.Minutes, sdcc->sTime.Seconds, sdcc->sDate.Date, sdcc->sDate.Month, sdcc->sDate.Year, sdcc->sDate.WeekDay, 0, STOP};
//					memcpy(SD_CARD_TIMESTAMP_DATA_BYTE_BUFFER, tmp, sizeof(SD_CARD_TIMESTAMP_DATA_BYTE_BUFFER));
//					BL_RFCOMM_Response(blrf, SD_CARD_TIMESTAMP_DATA_BYTE_BUFFER, sizeof(SD_CARD_TIMESTAMP_DATA_BYTE_BUFFER),1);
//				}
//				break;
//			}
//			case SDCARD_FORMAT: {
//				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
//
//				}
//				else {
//
//				}
//				break;
//			}
//			case SDCARD_OVERFLOW: {
//				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
//
//				}
//				else {
//
//				}
//				break;
//			}
//			default: return;
//			}
//			break;
		}
		case ADC_SRV: {
			ADC_Controller* adcc = ADC_GetADCController();
			switch(blap->blrf->receive_buffer[CHARACTERISTIC_BYTE_OFFSET]) {
			case ADC_RUNNING: { //DONE
				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_RUNNING, WRITE, _1_B, 0, STOP};
					if(blap->blrf->receive_buffer[REQUEST_DATA_BYTE_OFFSET]) tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_StartConversion(adcc)==HAL_OK ? 1 : 0;
					else tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_EndConversion(adcc)==HAL_OK ? 1 : 0;
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(BATTERY_SINGLE_DATA_BYTE_BUFFER),0);
				}
				else {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_RUNNING, READ, _1_B, adcc->adc_running, STOP};
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			case ADC_PAUSE: { //DONE
				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_PAUSE, WRITE, _1_B, 0, STOP};
					if(blap->blrf->receive_buffer[REQUEST_DATA_BYTE_OFFSET]) tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_TogglePauseConversion(adcc, 1)==HAL_OK ? 1 : 0;
					else tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_TogglePauseConversion(adcc, blap->blrf->receive_buffer[REQUEST_DATA_BYTE_OFFSET])==HAL_OK ? 1 : 0;
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				else {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_PAUSE, READ, _1_B, adcc->adc_pause, STOP};
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			case ADC_FREQUENCY: { //DONE
				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_FREQUENCY, WRITE, _1_B, 0, STOP};
					tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_UpdateSamplingFrequency(adcc, blap->blrf->receive_buffer[REQUEST_DATA_BYTE_OFFSET])==HAL_OK ? 1 : 0;
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				else {
					uint8_t tmp[7] = {START, ADC_SRV, ADC_FREQUENCY, READ, _1_B, ADC_GetFrequency(adcc), STOP};
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			case ADC_FREQUENCY_AVAILABLE: { //DONE
				if(!blap->blrf->receive_buffer[RW_BYTE_OFFSET]) {
					uint8_t tmp[14] = {START, ADC_SRV, ADC_FREQUENCY_AVAILABLE, READ, _8_B, 0,0,0,0,0,0,0,0, STOP};
					memcpy(tmp+RESPONSE_DATA_BYTE_OFFSET, ADC_TimerFrequenciesCodes, 8);
					memcpy(ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER, sizeof(ADC_FREQUENCY_AVAILABLE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			case ADC_CHANNELS_ACTIVE: { //DONE
				if(blap->blrf->long_data_receive_buffer[RW_BYTE_OFFSET]) { //WRITE //TODO: tu była zmiana?
					uint8_t tmp[7] = {START, ADC_SRV, ADC_CHANNELS_ACTIVE, WRITE, _1_B, 0, STOP};
					tmp[RESPONSE_DATA_BYTE_OFFSET] = ADC_UpdateChannels(adcc,blap->blrf->long_data_receive_buffer+REQUEST_DATA_BYTE_OFFSET)==HAL_OK ? 1 : 0;
					memcpy(ADC_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),0);
				}
				else {
					uint8_t tmp[14] = {START, ADC_SRV, ADC_CHANNELS_ACTIVE, READ, _8_B, 0,0,0,0,0,0,0,0, STOP};
					memcpy(tmp+RESPONSE_DATA_BYTE_OFFSET, adcc->active_channels, 8);
					memcpy(ADC_CHANNELS_ACTIVE_DATA_BYTE_BUFFER, tmp, sizeof(ADC_CHANNELS_ACTIVE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_CHANNELS_ACTIVE_DATA_BYTE_BUFFER, sizeof(ADC_CHANNELS_ACTIVE_DATA_BYTE_BUFFER),0);
				}
				break;
			}
			default: break;
			}
			break;
		}
		case CUSTOM_DATA_SRV: {
			switch(blap->blrf->receive_buffer[CHARACTERISTIC_BYTE_OFFSET]) {
			case CUSTOM_DATA_LENGTH: { //DONE
				if(blap->blrf->receive_buffer[RW_BYTE_OFFSET]) { //WRITE
					blap->blrf->long_data_f=1;
					blrf->pending_rq_f = 0;
					blap->blrf->long_data_length=DATA_LENGTH_BYTES_VALUES[blap->blrf->receive_buffer[REQUEST_DATA_BYTE_OFFSET]];
					uint8_t tmp[7] = {START, CUSTOM_DATA_SRV, CUSTOM_DATA_LENGTH, WRITE, _1_B, 1, STOP};
					memcpy(CUSTOM_DATA_SINGLE_DATA_BYTE_BUFFER, tmp, sizeof(CUSTOM_DATA_SINGLE_DATA_BYTE_BUFFER));
					BL_RFCOMM_Response(blrf, ADC_SINGLE_DATA_BYTE_BUFFER, sizeof(ADC_SINGLE_DATA_BYTE_BUFFER),1);
				}
				break;
			}
			default: break;
			}
			break;
		}
		default: return;
		}
	}
}
