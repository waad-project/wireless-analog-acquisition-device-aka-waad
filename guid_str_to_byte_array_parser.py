import sys
l = list(sys.argv[1]).reverse()
print(''.join(['0x'+ss+',' for ss in [str(l[i-2])+str(l[i-1]) for i in range(2,len(l)+2,2)]])[:-1])
